#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>

#include "hardwareAPI.h"

#define MAX_ELEVATORS 6

pthread_mutex_t lock;
pthread_mutex_t reset;
pthread_cond_t new_position;
pthread_cond_t new_task;
pthread_cond_t waiting;

double tasks[MAX_ELEVATORS][2];

void * Master(void * arg);
void * Elevator(void * arg);

int main(int argc, char **argv) {

  pthread_t master;
  pthread_t elevators[MAX_ELEVATORS];

  memset(tasks, 0, MAX_ELEVATORS);

  char *hostname;
  int port;
  long i;

  if (argc != 3) {
    fprintf(stderr, "Usage: %s host-name port\n", argv[0]);
    fflush(stderr);
    exit(-1);
  }
  hostname = argv[1];
  if ((port = atoi(argv[2])) <= 0) {
    fprintf(stderr, "Bad port number: %s\n", argv[2]);
    fflush(stderr);
    exit(-1);
  }
  initHW(hostname, port);

  if (pthread_mutex_init(&lock, NULL) < 0) {
    perror("pthread_mutex_init");
    exit(1);
  }

  if (pthread_cond_init(&new_task, NULL) < 0) {
    perror("pthread_cond_init");
    exit(1);
  }

  if (pthread_cond_init(&new_position, NULL) < 0) {
    perror("pthread_cond_init");
    exit(1);
  }

  if (pthread_mutex_init(&reset, NULL) < 0) {
    perror("pthread_mutex_init");
    exit(1);
  }

  if (pthread_create(&master, NULL, Master, (void *) 0) != 0) {
    perror("pthread_create");
    exit(-1);
  }

  for (i = 0; i < MAX_ELEVATORS; i++) {
    if (pthread_create(&elevators[i], NULL, Elevator, (void *) i)) {
      perror("pthread_create");
      exit(-1);
    }
  }

  (void) pthread_join(master, NULL);
  for (i = 0; i < MAX_ELEVATORS; i++)
    (void) pthread_join(elevators[i], NULL);

  return 0;
}

void * Master(void * arg) {
  EventType e;
  EventDesc ed;

  int broad_task, broad_pos;
  int direction, arrived = 0;

  while(1) {
    e = waitForEvent(&ed);
    switch (e) {
    case FloorButton:
      pthread_mutex_lock(&lock);
      fprintf(stdout, "floor button: floor %d, type %d\n",
	      ed.fbp.floor, (int) ed.fbp.type);
      fflush(stdout);
      pthread_mutex_unlock(&lock);
      break;

    case CabinButton:
      pthread_mutex_lock(&lock);
      fprintf(stdout, "cabin button: cabin %d, floor %d\n",
	      ed.cbp.cabin, ed.cbp.floor);
      fflush(stdout);
      tasks[ed.cbp.cabin][0] = 2;
      // tasks[ed.cbp.cabin][1] = ed.cp.position;
      tasks[ed.cbp.cabin][2] = ed.cbp.floor;
      pthread_mutex_lock(&reset);
      broad_task = pthread_cond_broadcast(&new_task);
      pthread_mutex_unlock(&reset);
      pthread_mutex_unlock(&lock);
      break;

    case Position:
      pthread_mutex_lock(&lock);
      fprintf(stdout, "cabin position: cabin %d, position %f\n",
	      ed.cp.cabin, ed.cp.position);
      fflush(stdout);
      tasks[ed.cp.cabin][0] = 3;
      tasks[ed.cp.cabin][1] = ed.cp.position;
      pthread_mutex_lock(&reset);
      broad_task = pthread_cond_broadcast(&new_task);
      pthread_mutex_unlock(&reset);
      pthread_mutex_unlock(&lock);
      break;

    case Speed:
      pthread_mutex_lock(&lock);
      fprintf(stdout, "speed: %f\n", ed.s.speed);
      fflush(stdout);
      pthread_mutex_unlock(&lock);
      break;

    case Error:
      pthread_mutex_lock(&lock);
      fprintf(stdout, "error: \"%s\"\n", ed.e.str);
      fflush(stdout);
      pthread_mutex_unlock(&lock);
      break;
    }
  }
}

void * Elevator(void * arg) {
  uintptr_t temp = (uintptr_t)arg;
  int id = (int) temp;
  int moving = 0;


  int task;
  double target;
  double pos;

  printf("ELEVATOR: %d CREATED\n", id);

  while(1) {
    pthread_mutex_lock(&reset);
    pthread_cond_wait(&new_task, &reset);
    pthread_mutex_unlock(&reset);
    task = (int)tasks[id][0];
    pos = tasks[id][1];
    target = tasks[id][2];

    switch (task) {
      case 1: // FloorButton
        // task
        break;
      case 2: // CabinButton
        if (target == 32000) {
          printf("STOP\n");
          handleMotor(id, MotorStop);
          moving = 0;
        }
        else if ((fabs(pos - target) < 0.00001) && !moving) {
        // else if ((*((int *)&target) == *((int *)&pos)) && moving) {
          printf("OPEN BTN\n");
          handleDoor(id, DoorOpen);
          sleep(3);
          printf("CLOSE BTN\n");
          handleDoor(id, DoorClose);
          tasks[id][0] = 0;
          moving = 0;
        }
        else if (pos < target) {
          printf("UP\n");
          handleMotor(id, MotorUp);
          moving = 1;
        }
        else if (pos > target) {
          printf("DOWN\n");
          handleMotor(id, MotorDown);
          moving = 1;
        }
        break;
      case 3: // Position
        if ((fabs(target - pos) <= 0.00001) && moving) {
        // if ((*((int *)&target) == *((int *)&pos)) && moving) {
          printf("%d\n", *(&pos));
          printf("%d\n", *(&target));
          tasks[id][0] = 0;
          handleMotor(id, MotorStop);
          moving = 0;
          printf("OPEN POS\n");
          handleDoor(id, DoorOpen);
          sleep(3);
          printf("CLOSE POS\n");
          handleDoor(id, DoorClose);
        }
        break;
      case 4: // Speed
        // task
        break;
      case 5:
        // task
        break;
      default:
        break;
    }
  }
}
